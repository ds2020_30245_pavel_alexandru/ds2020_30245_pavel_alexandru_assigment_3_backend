package backend.repositories;

import backend.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.Optional;
import java.util.UUID;

public interface DoctorRepo extends JpaRepository<Doctor, UUID> {

    Optional<Doctor> findById(UUID id);

    <S extends Doctor> S save(S s);

    void deleteById(UUID id);

}
