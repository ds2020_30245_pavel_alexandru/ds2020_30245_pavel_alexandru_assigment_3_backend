package backend.repositories;

import backend.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public interface PatientRepo extends JpaRepository<Patient, UUID> {

    Optional<Patient> findById(UUID id);

    ArrayList<Patient> findAll();

    ArrayList<Patient> findByName(String name);

    ArrayList<Patient> findByDob(Date date);

    ArrayList<Patient> findByGender(String gender);

    ArrayList<Patient> findByAddress(String address);

    ArrayList<Patient> findByMedRecord(String medRecord);

    <S extends Patient> S save(S s);

    void deleteById(UUID id);
}
