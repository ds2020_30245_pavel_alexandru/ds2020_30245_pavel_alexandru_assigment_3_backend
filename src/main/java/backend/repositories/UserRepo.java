package backend.repositories;

import backend.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface UserRepo extends JpaRepository<Users, UUID> {

    @Query(value = "SELECT p " +
            "FROM Users p " +
            "WHERE p.username = :username AND p.password = :password ")
    Optional<Users> findByUsernameAndPassword(@Param("username") String username,@Param("password") String password);

    Optional<Users> findById(UUID uuid);

    <S extends Users> S save(S s);

    void delete(Users user);

    @Transactional
    @Modifying
    @Query(value = "DELETE " +
            "FROM Users o " +
            "WHERE o.linkId = :accountId ")
    void deleteByAccount(@Param("accountId") UUID accountId);
}
