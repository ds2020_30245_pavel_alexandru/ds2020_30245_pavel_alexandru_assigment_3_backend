package backend.dtos.builders;

import backend.dtos.PatientDTO;
import backend.entities.Patient;

public class PatientBuilder {
    public static PatientDTO toPatientDTO(Patient patient) {
        return new PatientDTO(patient.getId(), patient.getName(),patient.getDob(), patient.getGender(), patient.getAddress(), patient.getMedRecord());
    }

    public static Patient toEntity(PatientDTO patientDTO)
    {
        return new Patient(patientDTO.getName(),patientDTO.getDob(),patientDTO.getGender(),patientDTO.getAddress(),patientDTO.getMedRecord());
    }
}
