package backend.dtos.builders;

import backend.dtos.ActivityDTO;
import backend.entities.Activities;

public class ActivityBuilder {

    public static ActivityDTO toActivityDTO(Activities activity) {
        return new ActivityDTO(activity.getId(), activity.getIdPatient(), activity.getName(), activity.getStart(), activity.getEnd());
    }

    public static Activities toActivity(ActivityDTO activityDTO) {
        return new Activities(activityDTO.getIdPatient(), activityDTO.getStart(), activityDTO.getEnd(), activityDTO.getName());
    }
}
