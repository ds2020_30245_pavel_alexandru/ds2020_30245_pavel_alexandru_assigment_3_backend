package backend.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class CaregiverDTO extends RepresentationModel<CaregiverDTO> {

    private UUID id;
    private String name;
    private Date dob;
    private String gender;
    private String address;
    private String patients;

    public CaregiverDTO(UUID id, String name, Date dob, String gender, String address, String patients)
    {
        this.id=id;
        this.name=name;
        this.dob=dob;
        this.gender=gender;
        this.address=address;
        this.patients=patients;
    }

    public UUID getId(){return id;}

    public void setId(UUID id){this.id=id;}

    public String getName(){return name;}

    public void setName(String name){this.name=name;}

    public Date getDob(){return  dob; }

    public void setDob(Date dob){this.dob=dob;}

    public String getGender(){return gender;}

    public void setGender(String gender){this.gender=gender;}

    public String getAddress(){return address;}

    public void setAddress(String address){this.address=address;}

    public String getPatients(){return patients;}

    public void setPatients(String patients)  {this.patients=patients;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CaregiverDTO caregiverDTO = (CaregiverDTO) o;
        return Objects.equals(id, caregiverDTO.id) &&
                Objects.equals(name, caregiverDTO.name) &&
                Objects.equals(dob, caregiverDTO.dob) &&
                Objects.equals(gender, caregiverDTO.gender) &&
                Objects.equals(address, caregiverDTO.address) &&
                Objects.equals(patients, caregiverDTO.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name,dob,gender,address,patients);
    }
}
