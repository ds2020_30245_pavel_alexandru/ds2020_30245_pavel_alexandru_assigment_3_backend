package backend.dtos;

import java.time.LocalDateTime;
import java.util.UUID;


public class ActivityDTO {

    private UUID id;
    private UUID idPatient;
    private String name;
    private LocalDateTime start;
    private LocalDateTime end;

    public ActivityDTO(UUID id, UUID idPatient, String name, LocalDateTime start, LocalDateTime end) {
        this.id = id;
        this.idPatient = idPatient;
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public ActivityDTO(UUID idPatient, String name, LocalDateTime start, LocalDateTime end) {
        this.idPatient = idPatient;
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public UUID getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(UUID idPatient) {
        this.idPatient = idPatient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }
}
