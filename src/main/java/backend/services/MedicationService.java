package backend.services;

import backend.dtos.MedicationDTO;
import backend.dtos.PatientDTO;
import backend.dtos.builders.MedicationBuilder;
import backend.dtos.builders.PatientBuilder;
import backend.entities.Medication;
import backend.entities.Patient;
import backend.entities.Users;
import backend.repositories.DoctorRepo;
import backend.repositories.MedicationRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final MedicationRepo medicationRepo;

    @Autowired
    public MedicationService(MedicationRepo medicationRepo) {this.medicationRepo=medicationRepo;}

    public MedicationDTO findById(UUID id) {

        Optional<Medication> medication=medicationRepo.findById(id);
        if (!medication.isPresent()) {
            LOGGER.error("Medication not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }
        return MedicationBuilder.medicationDTO(medication.get());
    }

    public ArrayList<MedicationDTO> findByName(String name) {

        ArrayList<Medication> medications = medicationRepo.findByName(name);
        if ( medications.isEmpty()) {
            LOGGER.error("Medications not found.");
        }
        return (ArrayList<MedicationDTO>) medications.stream().map(MedicationBuilder::medicationDTO).collect(Collectors.toList());
    }

    public ArrayList<MedicationDTO> findBySideEffects(String sideEffects) {

        ArrayList<Medication> medications = medicationRepo.findBySideEffects(sideEffects);
        if ( medications.isEmpty()) {
            LOGGER.error("Medications not found.");
        }
        return (ArrayList<MedicationDTO>) medications.stream().map(MedicationBuilder::medicationDTO).collect(Collectors.toList());
    }

    public ArrayList<MedicationDTO> findByDosage(String dosage) {

        ArrayList<Medication> medications = medicationRepo.findByDosage(dosage);
        if ( medications.isEmpty()) {
            LOGGER.error("Medications not found.");
        }
        return (ArrayList<MedicationDTO>) medications.stream().map(MedicationBuilder::medicationDTO).collect(Collectors.toList());
    }

    public ArrayList<MedicationDTO> findAll() {

        ArrayList<Medication> medications = medicationRepo.findAll();
        if ( medications.isEmpty()) {
            LOGGER.error("Medications not found.");
        }
        return (ArrayList<MedicationDTO>) medications.stream().map(MedicationBuilder::medicationDTO).collect(Collectors.toList());
    }

    public UUID insert(MedicationDTO medicationDTO) {
        Medication medication=MedicationBuilder.toEntity(medicationDTO);
        medication=medicationRepo.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
        return medication.getId();
    }

    public boolean delete(UUID id)
    {
        medicationRepo.deleteById(id);
        return true;
    }

    public boolean update(UUID id,MedicationDTO medicationDTO)
    {
        Optional<Medication> medication=medicationRepo.findById(id);
        if (!medication.isPresent()) {
            LOGGER.error("Medication not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }

        Medication medication1=medication.get();

        medication1.setName(medicationDTO.getName());
        medication1.setDosage(medicationDTO.getDosage());
        medication1.setSideEffects(medicationDTO.getSideEffects());
        medicationRepo.save(medication1);
        return true;
    }
}
