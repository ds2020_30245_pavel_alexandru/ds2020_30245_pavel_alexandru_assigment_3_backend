package backend.services;

import backend.dtos.ActivityDTO;
import backend.dtos.builders.ActivityBuilder;
import backend.entities.Activities;
import backend.repositories.ActivityRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ActivityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActivityService.class);
    private final ActivityRepo activityRepo;

    @Autowired
    public ActivityService(ActivityRepo activityRepo) {this.activityRepo= activityRepo;}

    public UUID insert(ActivityDTO activityDTO) {
        Activities activity = ActivityBuilder.toActivity(activityDTO);
        activity = activityRepo.save(activity);
        LOGGER.debug("Activity with id {} was inserted in db", activity.getId());
        return activity.getId();
    }

    public void dropAll() {
        activityRepo.deleteAll();
    }
}
