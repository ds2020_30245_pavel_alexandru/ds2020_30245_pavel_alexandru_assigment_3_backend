package backend.rabbitMQ;

import backend.dtos.ActivityDTO;
import backend.dtos.PatientDTO;
import backend.services.ActivityService;
import backend.services.PatientService;
import net.minidev.json.JSONObject;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Component
public class Receive {

    private ActivityService activityService;
    private PatientService patientService;

    @Autowired
    private SimpMessagingTemplate socket;

    @Autowired
    public Receive(ActivityService activityService,PatientService patientService) {
        this.activityService=activityService;
        this.patientService=patientService;
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue}", containerFactory = "containerFactory")

    public void receiveMessage(JSONObject activity1) {

        UUID patientId = UUID.fromString(activity1.getAsString("patient_id"));
        String activityName = activity1.getAsString("activity");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        String start1= activity1.getAsString("start");
        LocalDateTime start= LocalDateTime.parse(start1,formatter);

        String end1=activity1.getAsString("end");
        LocalDateTime end= LocalDateTime.parse(end1,formatter);

        if(!activityName.isEmpty()) {

            ActivityDTO activityDTO=new ActivityDTO(patientId,activityName,start,end);
            activityService.insert(activityDTO);

            PatientDTO patientDTO = patientService.findById(patientId);

            long time_spent = Duration.between(start, end).toMinutes();

            // sleep
            if (time_spent > 7 * 60 && activityName.equals("Sleeping"))
            {
                System.out.println("Sleeping period longer than 7 hours!");
                socket.convertAndSend("/topic/notify",String.format(patientDTO.getName()+" is sleeping for more than 7 hours!"));
            }

            // outdoor activity
            if (time_spent > 5 * 60 && activityName.equals("Leaving"))
            {
                System.out.println("Outdoor activity longer than 5 hours!");
                socket.convertAndSend("/topic/notify",String.format(patientDTO.getName()+" is outdoors for more than 5 hours!"));
            }

            // bathroom

            if (time_spent > 30 && (activityName.equals("Toileting") || activityName.equals("Showering") || activityName.equals("Grooming"))) {

                System.out.println("Period spent in bathroom longer than 30 minutes!");
                socket.convertAndSend("/topic/notify",String.format(patientDTO.getName()+" is in the bathroom for more than 30 minutes!"));
            }

        }

    }

}
