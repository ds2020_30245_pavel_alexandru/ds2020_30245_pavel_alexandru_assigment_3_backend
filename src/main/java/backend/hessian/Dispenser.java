package backend.hessian;

import backend.dtos.MedicationPlanDTO;
import backend.services.MedicationPlanService;
import net.minidev.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

public class Dispenser implements  DispenserInterface{

    private MedicationPlanService medicationPlanService;

    public Dispenser(MedicationPlanService medicationPlanService)
    {
        this.medicationPlanService=medicationPlanService;
    }

    @Override
    public ArrayList<JSONObject> getAllMedicationPlans(UUID id) {

        ArrayList<JSONObject> medicationplans = new ArrayList<JSONObject>();
        ArrayList<MedicationPlanDTO> medicationplansDTO = medicationPlanService.findByPatient(id);

        for(MedicationPlanDTO m : medicationplansDTO )
        {
            JSONObject jo = new JSONObject();
            jo.put("patientId",m.getPatientId());
            jo.put("meds",m.getMeds());
            jo.put("medsTime",m.getMedsTime());
            jo.put("startDate",m.getStartDate());
            jo.put("endDate",m.getEndDate());

            medicationplans.add(jo);
        }

        return medicationplans;
    }
}
