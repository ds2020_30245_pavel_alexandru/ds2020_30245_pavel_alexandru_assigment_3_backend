package backend.hessian;

import backend.services.MedicationPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;

@Configuration
public class ConfigHessian {

    @Autowired
    private MedicationPlanService medicationPlanService;

    @Bean(name = "/dispenser")
    RemoteExporter HessianService()
    {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(new Dispenser(medicationPlanService));
        exporter.setServiceInterface(DispenserInterface.class);
        return exporter;
    }

}
