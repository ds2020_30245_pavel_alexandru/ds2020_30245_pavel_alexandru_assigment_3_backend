package backend.controllers;

import backend.dtos.DoctorDTO;
import backend.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DoctorDTO> getDoctor(@PathVariable("id") UUID id) {

        DoctorDTO doctorDTO=doctorService.findById(id);
        return new ResponseEntity<>(doctorDTO, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertDoctor(@Valid @RequestBody DoctorDTO doctorDTO) {

        UUID id = doctorService.insert(doctorDTO);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteDoctor(@PathVariable("id") UUID id) {

        doctorService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateDoctor(@PathVariable("id") UUID accountId,@Valid @RequestBody DoctorDTO doctorDTO) {

        doctorService.update(accountId,doctorDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
